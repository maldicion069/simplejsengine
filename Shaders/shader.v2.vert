uniform mat4 uModelTransform;

attribute vec3 aSquareVertexPos;

void main(void) {
	gl_Position = uModelTransform * vec4(aSquareVertexPos, 1.0);
}