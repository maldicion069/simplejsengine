precision mediump float; // sets the precision for floating point computation

uniform vec4 ufragColor;

void main(void) {
	gl_FragColor = ufragColor;
}