uniform mat4 uModelTransform;
uniform mat4 uViewProjTransform;

attribute vec3 aSquareVertexPos;

void main(void) {
	gl_Position = uViewProjTransform * uModelTransform * vec4(aSquareVertexPos, 1.0);
}