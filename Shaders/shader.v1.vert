attribute vec3 aSquareVertexPos;

void main(void) {
	gl_Position = vec4(aSquareVertexPos, 1.0);
}