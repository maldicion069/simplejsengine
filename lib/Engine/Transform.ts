///<reference path="../../gl-matrix.d.ts" />

"use strict;"

class Transform {
	private mPosition : Float32Array;
	private mScale: Float32Array;
	private mRotationRad : number;
	private static _pi2 = 2*Math.PI;
	constructor() {
		this.mPosition = vec2.fromValues(0, 0);
		this.mScale = vec2.fromValues(1, 1);
		this.mRotationRad = 0.0;
	}
	public setPosition(xPos, yPos) {
		this.setXPos(xPos);
		this.setYPos(yPos);
	}
	public getXPos() { return this.mPosition[0]; }
	public getYPos() { return this.mPosition[1]; }
	public setXPos(xPos) {
		this.mPosition[0] = xPos;
	}
	public setYPos(yPos) {
		this.mPosition[1] = yPos;
	}
	public getPosition() {
		return this.mPosition;
	}
	public setSize(w, h) {
		this.setWidth(w);
		this.setHeight(h);
	}
	public getWidth() {
		return this.mScale[0];
	}
	public getHeight() {
		return this.mScale[1];
	}
	public setWidth(w) {
		this.mScale[0] = w;
	}
	public setHeight(h) {
		this.mScale[1] = h;
	}
	public getSize() {
		return this.mScale;
	}
	
	public incXPosBy(delta) { this.mPosition[0] += delta;}
	public incYPosBy(delta) { this.mPosition[1] += delta;}

	public setRotationInRad(rot) {
		this.mRotationRad = rot;
		while(this.mRotationRad > Transform._pi2) {
			this.mRotationRad -= Transform._pi2;
		}
	}
	public setRotationInDegree(rot) {
		this.setRotationInRad(rot * Math.PI / 180.0);
	}
	public getRotation() {
		return this.mRotationRad;
	}
	public incSizeBy(delta) {
		this.incWidthBy(delta);
		this.incHeightBy(delta);
	}
	public incWidthBy(dt) {
		this.mScale[0] += dt;
	}
	public incHeightBy(dt) {
		this.mScale[1] += dt;
	}
	public incRotationByDegree(deltaDegree) {
		this.incRotationByRad(deltaDegree * Math.PI / 180.0);
	}
	public incRotationByRad(deltaRad) {
		this.setRotationInRad(this.mRotationRad + deltaRad);
	}
	public getRotationInRad() {  return this.mRotationRad; }
	public calcTransf() {
		var matrix = mat4.create();
		
		// Se calculan las matrices al revés por la configuración de WebGL

		// Calculamos translación
		mat4.translate(matrix, matrix, vec3.fromValues(this.getXPos(), this.getYPos(), 0.0)); // Z = 0.0 por ser 2D
		// Concatenamos rotaciones
		mat4.rotateZ(matrix, matrix, this.getRotationInRad());
		// Concatenamos escalado
		mat4.scale(matrix, matrix, vec3.fromValues(this.getWidth(), this.getHeight(), 1.0));
		return matrix;
	}
}