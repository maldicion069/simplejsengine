///<reference path="../MyGame/MyGame.ts" />
///<reference path="Input.ts" />

class GameLoop {
	private kFPS: number;
	private kMPF: number;

	private mPreviousTime: number;
	private mLagTime: number;
	private mCurrentTime: number;
	private mElapsedTime: number;

	private mIsLoopRunning:boolean;

	private mMyGame: MyGame;
	
	constructor() {
		this.kFPS = 60;
		this.kMPF = 1000 / this.kFPS;
		this.mIsLoopRunning = false;
	}
	
	public start(myGame: MyGame) {
		this.mMyGame = myGame;

		this.mPreviousTime = Date.now();
		this.mLagTime = 0.0;

		this.mIsLoopRunning = true;

		requestAnimationFrame(() => this._runLoop());
	}
	
	public _runLoop() {
		if(this.mIsLoopRunning) {
			requestAnimationFrame(() => this._runLoop());
			
			this.mCurrentTime = Date.now();
			this.mElapsedTime = this.mCurrentTime - this.mPreviousTime;
			this.mPreviousTime = this.mCurrentTime;
			this.mLagTime += this.mElapsedTime;
			
			while((this.mLagTime >= this.kMPF) && this.mIsLoopRunning) {
				Input.getInstance().update();
				this.mMyGame.update();
				this.mLagTime -= this.kMPF;
			}
			//console.log("Llamo a pintar");
			if(!this.mMyGame.isPause()) {
				this.mMyGame.draw();
			}
		}
	}
}