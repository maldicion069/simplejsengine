///<reference path="Core.ts" />
///<reference path="SimpleGLShader.ts" />
///<reference path="Transform.ts" />

"use strict";

class Drawable {
	private mShader: SimpleGLShader;
	private mColor: number[];
	private mTransform: Transform;
	get color():number[] {
		return this.mColor;
	}
	set color(color: number[]) {
		this.mColor = color;
	}
	constructor(shader: SimpleGLShader) {
		this.mShader = shader;
		this.mColor = [1, 1, 1, 1];
		this.mTransform = new Transform();
	}
	get transform(): Transform {
		return this.mTransform;
	}
	public draw(vpMatrix) {
		var gl = EngineCore.getInstance().getGL();
		this.mShader.activeShader(this.mColor, vpMatrix);
		//console.log(this.mTransform.calcTransf());
		this.mShader.loadObjectTransform(this.mTransform.calcTransf());
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
	}
}