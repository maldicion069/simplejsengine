///<reference path="Core.ts" />

"use strict";

class SimpleGLShader {
	
	private mCompiledShader: WebGLProgram;
    private mShaderVertexPosAttr : number;
	private mFragColor: WebGLUniformLocation;
	private mModelTransf: WebGLUniformLocation;
	private mViewProjTransf: WebGLUniformLocation;
	
	constructor(vShader: string, fShader: string, isFile: boolean = false) {
		
		var gl = EngineCore.getInstance().getGL();
		
		var vertexShader : WebGLShader, fragmentShader : WebGLShader;
		if(isFile) {
			vertexShader = this.loadAndCompileWithFile(vShader, gl.VERTEX_SHADER);
			fragmentShader = this.loadAndCompileWithFile(fShader, gl.FRAGMENT_SHADER);
		} else {
			vertexShader = this.loadAndCompile(vShader, gl.VERTEX_SHADER);
			fragmentShader = this.loadAndCompile(fShader, gl.FRAGMENT_SHADER);
		}
		
		// Creamos y linkamos shaders
		this.mCompiledShader = gl.createProgram();
		gl.attachShader(this.mCompiledShader, vertexShader);
		gl.attachShader(this.mCompiledShader, fragmentShader);
		gl.linkProgram(this.mCompiledShader);
		
		// Consultamos errores
		if(!gl.getProgramParameter(this.mCompiledShader, gl.LINK_STATUS)) {
			alert("ERROR");
			return;
			//throw;
		}
		
		// Obtenemos la referencia a "aSquareVertexPos"
		this.mShaderVertexPosAttr = gl.getAttribLocation(this.mCompiledShader, "aSquareVertexPos");
		this.mFragColor = gl.getUniformLocation(this.mCompiledShader, "ufragColor");
		this.mModelTransf = gl.getUniformLocation(this.mCompiledShader, "uModelTransform");
		this.mViewProjTransf = gl.getUniformLocation(this.mCompiledShader, "uViewProjTransform");
		
		gl.vertexAttribPointer(this.mShaderVertexPosAttr, 
			3,				// vec3
			gl.FLOAT,		// float
			false,			// no es normalizado
			0,				// número de bytes de separación entre elementos
			0				// offset del primer elemento
		);
	}
	
	public loadObjectTransform(modelTransform) {
		var gl = EngineCore.getInstance().getGL();
		gl.uniformMatrix4fv(this.mModelTransf, false, modelTransform);
	}
	
	private loadAndCompileWithFile(filePath: string, shaderType: number) {
		var request  : XMLHttpRequest = new XMLHttpRequest();
		request.open("GET", filePath, false);
		try {
			request.send();
		} catch(err) {
			alert("ERROR: " + filePath);
			return null;
		}
		var shaderSource : string = request.responseText;
		if(shaderSource === null) {
			alert("WARNING: " + filePath + " failed");
			return null;
		}
		
		return this.compileShader(shaderSource, shaderType);
	}
	
	private loadAndCompile(id: string, shaderType: number) {
		var shaderText : HTMLElement, shaderSource : string;
		
		var gl = EngineCore.getInstance().getGL();
		
        // Obtenemos el shader del index.html
        shaderText = document.getElementById(id);
        shaderSource = shaderText.firstChild.textContent;
		
		if(shaderSource === null) {
			alert("WARNING: " + id + " failed");
			return null;
		}
		
		return this.compileShader(shaderSource, shaderType);
	}
	
	private compileShader(shaderSource: string, shaderType: number) {
		var compiledShader : WebGLShader;
		
		var gl = EngineCore.getInstance().getGL();
        // Creamos el shader
        compiledShader = gl.createShader(shaderType);
        
        // Compilamos el shader
        gl.shaderSource(compiledShader, shaderSource);
        gl.compileShader(compiledShader);
        
        // Consultamos si hay errores
        if(!gl.getShaderParameter(compiledShader, gl.COMPILE_STATUS)) {
            alert("ERROR: " + gl.getShaderInfoLog(compiledShader));
        }
        return compiledShader;
	}
	
public activeShader(color: number[], vpMatrix) {
		var gl = EngineCore.getInstance().getGL();
		gl.useProgram(this.mCompiledShader);
		gl.uniformMatrix4fv(this.mViewProjTransf, false, vpMatrix);
		gl.enableVertexAttribArray(this.mShaderVertexPosAttr);
		gl.uniform4fv(this.mFragColor, color);
	}
}