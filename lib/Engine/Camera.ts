///<reference path="Core.ts" />
///<reference path="../../gl-matrix.d.ts" />

class Camera {
		
	private mWCCenter;
	private mWCWidth;
	private mViewPort;
	private mNearPlane;
	private mFarPlane;
	private mViewMatrix;
	private mProjMatrix;
	private mVPMatrix;
	private mBGColor;

	constructor(wcCenter, wcWidth, viewportArr) {
		this.mWCCenter = wcCenter;
		this.mWCWidth = wcWidth;
		this.mViewPort = viewportArr; // [x, y, width, height]
		this.mNearPlane = 0;
		this.mFarPlane = 1000;
		
		// Transform matrix
		this.mViewMatrix = mat4.create();
		this.mProjMatrix = mat4.create();
		this.mVPMatrix = mat4.create();
		
		// Bg color
		this.mBGColor = [0.8, 0.8, 0.8, 1];
	}
	public getWCCenter() {return this.mWCCenter;}
	public setWCWidth(w) { this.mWCWidth = w;}
	public setViewport(vArr) { this.mViewPort = vArr;}
	public getViewport() { return this.mViewPort;}
	public setBgColor(nColor) { this.mBGColor = nColor;}
	public getBgColor() { return this.mBGColor;}
	public getVPMatrix() { return this.mVPMatrix;}
	public setWCCenter(xPos, yPos) {
		this.mWCCenter[0] = xPos;
		this.mWCCenter[1] = yPos;
	}
	
	
	public setupViewProjection() {
		var gl = EngineCore.getInstance().getGL();
		// Configuramos Viewport
		gl.viewport(this.mViewPort[0], this.mViewPort[1], this.mViewPort[2], this.mViewPort[3]);
		gl.scissor(this.mViewPort[0], this.mViewPort[1], this.mViewPort[2], this.mViewPort[3]);
		gl.clearColor(this.mBGColor[0], this.mBGColor[1], this.mBGColor[2], this.mBGColor[3]);
		gl.enable(gl.SCISSOR_TEST);
		gl.clear(gl.COLOR_BUFFER_BIT);
		gl.disable(gl.SCISSOR_TEST);
		
		// Definimos matrix View-Projection
		var cameraPosition = new Float32Array([this.mWCCenter[0], this.mWCCenter[1], 10]);
		var lookAtPosition = new Float32Array([this.mWCCenter[0], this.mWCCenter[1], 0]);
		var orientation = new Float32Array([0, 1, 0]);
		mat4.lookAt(this.mViewMatrix, cameraPosition, lookAtPosition, orientation);
		
		var halfWCWidth = 0.5 * this.mWCWidth;
		var halfWCHeight = halfWCWidth * this.mViewPort[3] / this.mViewPort[2];
			// WCHeight = WCWidth * viewportHeight / viewportWidth
			mat4.ortho(this.mProjMatrix,
			-halfWCWidth, // distant to left of WC
			halfWCWidth, // distant to right of WC
			-halfWCHeight, // distant to bottom of WC
			halfWCHeight, // distant to top of WC
		this.mNearPlane, // z-distant to near plane
			this.mFarPlane); // z-distant to far plane);
		// Step B3: concatnate view and project matrices
		mat4.multiply(this.mVPMatrix, this.mProjMatrix, this.mViewMatrix);
	}
}