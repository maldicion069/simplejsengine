"use strict;";
var Transform = (function () {
    function Transform() {
        this.mPosition = vec2.fromValues(0, 0);
        this.mScale = vec2.fromValues(1, 1);
        this.mRotationRad = 0.0;
    }
    Transform.prototype.setPosition = function (xPos, yPos) {
        this.setXPos(xPos);
        this.setYPos(yPos);
    };
    Transform.prototype.getXPos = function () {
        return this.mPosition[0];
    };
    Transform.prototype.getYPos = function () {
        return this.mPosition[1];
    };
    Transform.prototype.setXPos = function (xPos) {
        this.mPosition[0] = xPos;
    };
    Transform.prototype.setYPos = function (yPos) {
        this.mPosition[1] = yPos;
    };
    Transform.prototype.getPosition = function () {
        return this.mPosition;
    };
    Transform.prototype.setSize = function (w, h) {
        this.setWidth(w);
        this.setHeight(h);
    };
    Transform.prototype.getWidth = function () {
        return this.mScale[0];
    };
    Transform.prototype.getHeight = function () {
        return this.mScale[1];
    };
    Transform.prototype.setWidth = function (w) {
        this.mScale[0] = w;
    };
    Transform.prototype.setHeight = function (h) {
        this.mScale[1] = h;
    };
    Transform.prototype.getSize = function () {
        return this.mScale;
    };

    Transform.prototype.incXPosBy = function (delta) {
        this.mPosition[0] += delta;
    };
    Transform.prototype.incYPosBy = function (delta) {
        this.mPosition[1] += delta;
    };

    Transform.prototype.setRotationInRad = function (rot) {
        this.mRotationRad = rot;
        while (this.mRotationRad > Transform._pi2) {
            this.mRotationRad -= Transform._pi2;
        }
    };
    Transform.prototype.setRotationInDegree = function (rot) {
        this.setRotationInRad(rot * Math.PI / 180.0);
    };
    Transform.prototype.getRotation = function () {
        return this.mRotationRad;
    };
    Transform.prototype.incSizeBy = function (delta) {
        this.incWidthBy(delta);
        this.incHeightBy(delta);
    };
    Transform.prototype.incWidthBy = function (dt) {
        this.mScale[0] += dt;
    };
    Transform.prototype.incHeightBy = function (dt) {
        this.mScale[1] += dt;
    };
    Transform.prototype.incRotationByDegree = function (deltaDegree) {
        this.incRotationByRad(deltaDegree * Math.PI / 180.0);
    };
    Transform.prototype.incRotationByRad = function (deltaRad) {
        this.setRotationInRad(this.mRotationRad + deltaRad);
    };
    Transform.prototype.getRotationInRad = function () {
        return this.mRotationRad;
    };
    Transform.prototype.calcTransf = function () {
        var matrix = mat4.create();

        mat4.translate(matrix, matrix, vec3.fromValues(this.getXPos(), this.getYPos(), 0.0));

        mat4.rotateZ(matrix, matrix, this.getRotationInRad());

        mat4.scale(matrix, matrix, vec3.fromValues(this.getWidth(), this.getHeight(), 1.0));
        return matrix;
    };
    Transform._pi2 = 2 * Math.PI;
    return Transform;
})();
//# sourceMappingURL=Transform.js.map
