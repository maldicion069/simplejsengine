///<reference path="Input.ts" />

"use strict";

class EngineCore {
	private static _instance: EngineCore = new EngineCore();
	private _mGL: WebGLRenderingContext;
	constructor() {
		if(EngineCore._instance) {
			// error
		}
		EngineCore._instance = this;
		Input.getInstance().initialize();
	}
	public static getInstance():EngineCore {
		return EngineCore._instance;
	}
	public getGL() {
		return this._mGL;
	}
	public initialize(htmlCanvasID: string) {
		var canvas : any = document.getElementById(htmlCanvasID);
		this._mGL = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
		
		if(this._mGL === null) {
			alert("WEBGL is not suported");
		}
		
		// now initialize the VertexBuffer
		this.VertexBuffer();
 		//gEngine.VertexBuffer.initialize();
		
	}
	public clearColor(color: number[]) {
		this._mGL.clearColor(color[0], color[1], color[2], color[3]);
		this._mGL.clear(this._mGL.COLOR_BUFFER_BIT);
	}
	
	public VertexBuffer() {
		// reference to the vertex positions for the square in the gl context
		var mSquareVertexBuffer = null;

		// First: define the vertices for a square
		var verticesOfSquare = [
			0.5, 0.5, 0.0,
			-0.5, 0.5, 0.0,
			0.5, -0.5, 0.0,
			-0.5, -0.5, 0.0
		];
		var gl = this.getGL();

        // Step A: Create a buffer on the gGL context for our vertex positions
        mSquareVertexBuffer = gl.createBuffer();

        // Step B: Activate vertexBuffer
        gl.bindBuffer(gl.ARRAY_BUFFER, mSquareVertexBuffer);

        // Step C: Loads verticesOfSquare into the vertexBuffer
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verticesOfSquare), gl.STATIC_DRAW);
	}
}