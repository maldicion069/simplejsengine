module Utils {
	export function SceneFileParser(filePath) {
		var request  : XMLHttpRequest = new XMLHttpRequest();
		request.open("GET", filePath, false);
		try {
			request.send();
		} catch(err) {
			alert("ERROR: " + filePath);
			return null;
		}
		var sceneText : string = request.responseText;
		console.log(JSON.parse(sceneText));
	}
};