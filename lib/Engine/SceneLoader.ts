///<reference path="Camera.ts" />
///<reference path="Drawable.ts" />

class SceneLoader {
	private mConfig:JSON;
	constructor(filePath) {
		var request  : XMLHttpRequest = new XMLHttpRequest();
		request.open("GET", filePath, false);
		try {
			request.send();
		} catch(err) {
			alert("ERROR: " + filePath);
			return null;
		}
		var sceneText : string = request.responseText;
		this.mConfig = JSON.parse(sceneText);
		console.log(this.mConfig);
	}
	public parseCamera() {
		var camera = this.mConfig["camera"];
		var cx = camera["centerX"];
		var cy = camera["centerY"];
		var w = camera["width"];
		var viewport = camera["viewport"];
		var bgColor = camera["bg"];
		
		var cam = new Camera(
		vec2.fromValues(cx, cy),
			w,
			viewport
		);
		cam.setBgColor(bgColor);
		return cam;
	}
	
	public parseObjects(objects: Drawable[], shader: SimpleGLShader) {
		if(!objects) {
			objects = [];
		}
		var pos, width, height, rotation, color, obj;
		for(var i = 0; i < this.mConfig["objects"].length; i++) {
			obj = this.mConfig["objects"][i];
			pos = obj["pos"];
			width = obj["width"];
			height = obj["height"];
			rotation = obj["rotation"];
			color = obj["color"];
			var elem = new Drawable(shader);
			elem.color = color;
			elem.transform.setPosition(pos["x"], pos["y"]);
			elem.transform.setRotationInDegree(rotation);
			elem.transform.setSize(width, height);
			objects.push(elem);
		}
	}
}

//{"id": "Square1", "pos": {"x": 20, "y": 60}, "width": 5, "height": 5, "rotation": 30, "color": [1, 1, 1, 1]},