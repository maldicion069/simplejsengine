///<reference path="../Engine/Core.ts" />
///<reference path="../Engine/SimpleGLShader.ts" />
///<reference path="../Engine/Camera.ts" />
///<reference path="../Engine/Drawable.ts" />
///<reference path="../Engine/GameLoop.ts" />
///<reference path="../Engine/SceneLoader.ts" />
///<reference path="../../gl-matrix.d.ts" />

class MyGame {
	private mCamera: Camera;
	private mShader: SimpleGLShader;
	private rect1: Drawable;
	private rect2: Drawable;
	private mActive: boolean = true;
	private mGameLoop: GameLoop;
	private objects: Drawable[] = [];
	constructor(htmlCanvasID: string) {
		this.mShader = null;
		
		EngineCore.getInstance().initialize(htmlCanvasID);
		
		this.init();
		
		/*var mCamera = new Camera(vec2.fromValues(20, 60), 20, [20, 40, 600, 300]);
		mCamera.setupViewProjection();

		var vpMatrix = mCamera.getVPMatrix();
		
		rect1.transform.setPosition(20, 60);
		rect1.transform.setRotationInRad(0.2);
		rect1.transform.setSize(5, 5);
		rect1.draw(vpMatrix);
		
		rect2.transform.setPosition(20, 60);
		rect2.transform.setSize(2, 2);
		rect2.draw(vpMatrix);*/
	}
	private init() {
		this.mShader = new SimpleGLShader("../Shaders/shader.v3.vert", "../Shaders/shader.v3.frag", true);
		var scene: SceneLoader = new SceneLoader("../scene.json");
		
		//this.mCamera = new Camera(vec2.fromValues(20, 60), 20, [20, 40, 600, 300]);
		//this.mCamera.setBgColor([0.8, 0.2, 1.0, 1.0]);
		this.mCamera = scene.parseCamera();
		
		scene.parseObjects(this.objects, this.mShader);
		
		// Create Drawable Objects
		this.rect1 = new Drawable(this.mShader);
		this.rect1.color = [0.25, 0.95, 1];
		
		this.rect2 = new Drawable(this.mShader);
		this.rect2.color = [0.95, 0.25, 1];
		
		this.rect1.transform.setPosition(20, 60);
		this.rect1.transform.setRotationInRad(0.2);
		this.rect1.transform.setSize(5, 5);
		
		this.rect2.transform.setPosition(30, 60);
		this.rect2.transform.setSize(2, 2);
		
		this.mGameLoop = new GameLoop();
		this.mGameLoop.start(this);
	}
	
	public update() {
		/**
		if(this.mActive) {
			var transf1 = this.rect1.transform;
			var transf2 = this.rect2.transform;
			var deltaX = 0.05;

			if(Input.getInstance().isKeyPressed(kKeys.Right)) {
				//console.log("R");
				if(transf1.getXPos() > 30) {
					transf1.setPosition(10, 60);
				}
				transf1.incXPosBy(deltaX);
			}
			if(Input.getInstance().isKeyPressed(kKeys.Left)) {
				//console.log("L");
				if(transf1.getXPos() < 10) {
					transf1.setPosition(30, 60);
				}
				transf1.incXPosBy(-deltaX);
			}
			if(Input.getInstance().isKeyPressed(kKeys.Up)) {
				//console.log("U");
				if(transf1.getYPos() < 10) {
					transf1.setPosition(30, 60);
				}
				transf1.incYPosBy(deltaX);
			}
			if(Input.getInstance().isKeyPressed(kKeys.Down)) {
				//console.log("D");
				if(transf1.getYPos() < 10) {
					transf1.setPosition(30, 60);
				}
				transf1.incYPosBy(-deltaX);
			}

			if(Input.getInstance().isKeyPressed(kKeys.A)) {
				transf2.incSizeBy(0.05);
			}
			if(Input.getInstance().isKeyPressed(kKeys.S)) {
				transf2.incSizeBy(-0.05);
			}
			if(Input.getInstance().isKeyPressed(kKeys.Q)) {
				transf1.incRotationByDegree(1);
				transf2.incRotationByDegree(-1);
			}
			if(Input.getInstance().isKeyPressed(kKeys.W)) {
				transf1.incRotationByDegree(-1);
				transf2.incRotationByDegree(1);
			}
			if(Input.getInstance().isKeyPressed(kKeys.F)) {
				// Pausa
				this.mActive = false;
			}
		} else {
			if(Input.getInstance().isKeyPressed(kKeys.F)) {
				// Resume
				this.mActive = true;
			}
		}
		//*/
	}
	
	public isPause():boolean {
		return !this.mActive;
	}
	
	public draw() {
		//console.log("Pintando");
		EngineCore.getInstance().clearColor([0, 0.8, 0, 1]);
		this.mCamera.setupViewProjection();
		for(var i = 0; i < this.objects.length; i++) {
			this.objects[i].draw(this.mCamera.getVPMatrix());
		}

		//this.rect1.draw(this.mCamera.getVPMatrix());
		//this.rect2.draw(this.mCamera.getVPMatrix());
	}
}

new MyGame("myCanvas");