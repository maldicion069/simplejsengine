var MyGame = (function () {
    function MyGame(htmlCanvasID) {
        this.mActive = true;
        this.objects = [];
        this.mShader = null;

        EngineCore.getInstance().initialize(htmlCanvasID);

        this.init();
    }
    MyGame.prototype.init = function () {
        this.mShader = new SimpleGLShader("../Shaders/shader.v3.vert", "../Shaders/shader.v3.frag", true);
        var scene = new SceneLoader("../scene.json");

        this.mCamera = scene.parseCamera();

        scene.parseObjects(this.objects, this.mShader);

        this.rect1 = new Drawable(this.mShader);
        this.rect1.color = [0.25, 0.95, 1];

        this.rect2 = new Drawable(this.mShader);
        this.rect2.color = [0.95, 0.25, 1];

        this.rect1.transform.setPosition(20, 60);
        this.rect1.transform.setRotationInRad(0.2);
        this.rect1.transform.setSize(5, 5);

        this.rect2.transform.setPosition(30, 60);
        this.rect2.transform.setSize(2, 2);

        this.mGameLoop = new GameLoop();
        this.mGameLoop.start(this);
    };

    MyGame.prototype.update = function () {
    };

    MyGame.prototype.isPause = function () {
        return !this.mActive;
    };

    MyGame.prototype.draw = function () {
        EngineCore.getInstance().clearColor([0, 0.8, 0, 1]);
        this.mCamera.setupViewProjection();
        for (var i = 0; i < this.objects.length; i++) {
            this.objects[i].draw(this.mCamera.getVPMatrix());
        }
    };
    return MyGame;
})();

new MyGame("myCanvas");
//# sourceMappingURL=MyGame.js.map
