module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-typescript');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        typescript: {
            base: {
                src: ["!**/*.d.ts", 'lib/**/*.ts'],
                options: {
                	compile: true,
                    module: 'commonjs',
                    target: 'es5',
                    sourceMap: true,
					declaration: false,		// Generate .d.ts
					keepDirectoryHierarchy: true
                }
            }
        },
        watch: {
            files: ["!**/*.d.ts", 'lib/**/*.ts'],
            tasks: ['typescript']
        }
    });
 
    //grunt.registerTask('default', ['typescript']);
    grunt.registerTask('default', ['typescript', 'watch']);
    
};